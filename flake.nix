{
  description = "NixOS Configuration for the CI Box";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    secrets.url = "git+ssh://gitea@git.kotatsu.dev:3080/kotatsuyaki/server-nixos-config-secrets.git";
    agenix.url = "github:ryantm/agenix";
    agenix.inputs.nixpkgs.follows = "nixpkgs";
    agenix.inputs.darwin.follows = "";
    tg-sticker-bot.url = "git+https://codeberg.org/kotatsuyaki/telegram-sticker-search-bot.git";
    tg-sticker-bot.inputs.nixpkgs.follows = "nixpkgs";
    tg-moeru-bot.url = "git+https://codeberg.org/kotatsuyaki/telegram-moeru-bot.git";
    tg-moeru-bot.inputs.nixpkgs.follows = "nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, agenix, flake-utils, ... } @ inputs:
    {
      nixosConfigurations = let
        lib = nixpkgs.lib;
        pkgs = nixpkgs.legacyPackages.x86_64-linux;

        base-modules = import ./base { inherit lib pkgs; };
        cibox-modules = [(import ./secrets/age.nix { inherit lib pkgs; })] ++ base-modules ++ import ./cibox { inherit lib pkgs; };
        cirunner-modules = base-modules ++ import ./cirunner { inherit lib pkgs; };
      in
        {
          # The VPS.
          cibox = nixpkgs.lib.nixosSystem {
            system = "x86_64-linux";
            specialArgs = { inherit self inputs; };
            modules = cibox-modules ++ [
              agenix.nixosModules.default
            ];
          };

          # The local runner.
          cirunner = nixpkgs.lib.nixosSystem {
            system = "x86_64-linux";
            specialArgs = { inherit self inputs; };
            modules = cirunner-modules;
          };
        };

      devShells = flake-utils.lib.eachDefaultSystemMap (system: let
        pkgs = nixpkgs.legacyPackages.${system};
      in
        {
          default = pkgs.mkShell {
            packages = [
              agenix.packages.${system}.default
            ];
          };
        });

      packages = flake-utils.lib.eachDefaultSystemMap (system: let
        pkgs = nixpkgs.legacyPackages.${system};
      in
        {
          ibm-plex-webfonts = pkgs.callPackage ./packages/ibm-plex-webfonts.nix { };
          iosevka-webfonts = pkgs.callPackage ./packages/iosevka-webfonts.nix { };
        });
    };
}
