{ pkgs, inputs, ... }: {
  imports = [
    inputs.tg-moeru-bot.nixosModules.default
  ];

  environment.systemPackages = [
    pkgs.btrfs-progs
    pkgs.btop
  ];

  networking.firewall = {
    enable = true;
    allowedTCPPorts = [ ];
    pingLimit = "--limit 1/s --limit-burst 3";
  };

  services.moerud.enable = true;

  services.searx = {
    enable = true;
    environmentFile = "/secrets/searx.env";
    settings = {
      server.bind_address = "127.0.0.1";
      server.port = inputs.secrets.searx-port;
      server.secret_key = "@SEARX_SECRET_KEY@";

      use_default_settings.engines.keep_only = [
        "google"
        "duckduckgo"
        "bing"
      ];

      search.formats = [ "html" "json" ];

      engines = [{
        name = "google";
        tokens = [ inputs.secrets.searx-token ];
      } {
        name = "duckduckgo";
        tokens = [ inputs.secrets.searx-token ];
      } {
        name = "bing";
        tokens = [ inputs.secrets.searx-token ];
      }];
    };
  };

  virtualisation.podman = {
    enable = true;
    autoPrune.enable = true;
    autoPrune.dates = "daily";
    defaultNetwork.settings.dns_enable = true;
  };

  networking.firewall.interfaces."podman+" = {
    allowedUDPPorts = [ 53 ];
    allowedTCPPorts = [ 53 ];
  };
}
