{ inputs, ... }: {
  networking = {
    hostName = "cirunner";
    useDHCP = false;
    defaultGateway.address = inputs.secrets.cirunner-gateway;
    nameservers = [ "8.8.8.8" "8.8.4.4" ];
    interfaces.eth0.ipv4.addresses = [{
      address = inputs.secrets.cirunner-address;
      prefixLength = 24;
    }];
  };
}
