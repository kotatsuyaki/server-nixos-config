{ inputs, ... }:
let
  inherit (inputs) self nixpkgs;
in
{
  nix.settings.auto-optimise-store = true;
  nix.gc = {
    automatic = true;
    dates = "weekly";
  };
  system.stateVersion = "22.11";

  nix.registry.nixpkgs.flake = nixpkgs;
  nix.registry.active-config.flake = self;
  environment.etc."nix/active-config".source = self;
}
