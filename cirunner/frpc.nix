# frp (fast reverse tunnel) client

{ pkgs, inputs, ... }:

let
  frp-client-cfg-file = pkgs.writeText "frp-client.ini" ''
    [common]
    server_addr = ${inputs.secrets.cibox-address}
    server_port = 49999
    authentication_method = token
    token = {{ .Envs.FRP_TOKEN }}

    [searx]
    type = tcp
    local_ip = 127.0.0.1
    local_port = ${toString inputs.secrets.searx-port}
    remote_port = ${toString inputs.secrets.searx-port}
    use_encryption = true
  '';
in
{
  systemd.services.frp-client = {
    wantedBy = [ "multi-user.target" ];
    after = [ "network.target" ];
    description = "Start the frp client";
    serviceConfig = {
      ExecStart = "${pkgs.frp}/bin/frpc -c ${frp-client-cfg-file}";
      EnvironmentFile = "/var/lib/frp-client.env";
      Type = "simple";
      Restart = "always";
      RestartSec = 10;
      DynamicUser = true;
    };
  };
}
