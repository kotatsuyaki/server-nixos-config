{ pkgs, lib, ... }: [
  ./nix.nix
  ./hardware.nix
  ./networking.nix
  ./application.nix
  ./frpc.nix
  {
    documentation.enable = false;
    i18n.supportedLocales = lib.mkForce [ "en_US.UTF-8/UTF-8" ];
  }
]
