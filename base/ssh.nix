{ config, inputs, ... }: {
  services.openssh = {
    enable = true;
    ports = [ inputs.secrets.ssh-port ];
    settings = {
      PasswordAuthentication = false;
    };
  };

  services.fail2ban = {
    enable = true;
  };

  users.users.root.openssh.authorizedKeys.keys = with (import ../secrets/keys.nix); [
    hydrogen
  ];
}
