{ modulesPath, ... }: {
  imports = [
    (modulesPath + "/profiles/qemu-guest.nix")
  ];

  zramSwap = {
    enable = true;
    # Higher than /dev/vda2 so that zram gets filled first
    priority = 5;
  };

  boot.loader.grub.device = "/dev/vda";
  boot.initrd.availableKernelModules = [
    "ata_piix"
    "uhci_hcd"
    "xen_blkfront"
    "vmw_pvscsi"
  ];
  boot.initrd.kernelModules = [
    "nvme"
  ];

  fileSystems."/" = {
    device = "/dev/vda1";
    fsType = "btrfs";
    options = [
      "compress=zstd"
    ];
  };

  swapDevices = [
    {
      device = "/dev/vda2";
      priority = 3;
    }
  ];
}
