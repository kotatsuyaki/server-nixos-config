{ pkgs, lib, ... }: [
  ./hardware.nix
  ./networking.nix
  ./nix.nix
  ./application.nix
  ./stickerbot.nix
  ./frp-server.nix
  {
    documentation.enable = false;
    i18n.supportedLocales = lib.mkForce [ "en_US.UTF-8/UTF-8" ];
  }
]
