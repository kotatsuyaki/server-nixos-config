{ pkgs, ... }:

let
  frp-server-cfg-file = pkgs.writeText "frp-server.ini" ''
    [common]
    bind_addr = 0.0.0.0
    bind_port = 49999
    authentication_method = token
    token = {{ .Envs.FRP_TOKEN }}
  '';
in
{
  systemd.services.frp-server = {
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      description = "Start the frp server";
      serviceConfig = {
        ExecStart = "${pkgs.frp}/bin/frps -c ${frp-server-cfg-file}";
        EnvironmentFile = "/var/lib/frp-server.env";
        Type = "simple";
        Restart = "always";
        RestartSec = 10;
        DynamicUser = true;
      };
   };
}
