{ inputs, ... }:
let
  inherit (inputs) self nixpkgs;
in
{
  nix.settings.auto-optimise-store = true;
  nix.gc = {
    automatic = true;
    dates = "weekly";
  };
  system.stateVersion = "23.05";

  nix.registry.nixpkgs.flake = nixpkgs;
  nix.registry.active-config.flake = self;
  environment.etc."nix/active-config".source = self;
}
