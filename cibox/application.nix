{ pkgs, config, self, inputs, ... }: {
  imports = [
    ../modules/ensure-secrets.nix
  ];

  environment.systemPackages = [
    pkgs.btrfs-progs
    pkgs.btop
    pkgs.mox
  ];

  users.users.mox = {
    name = "mox";
    home = "/home/mox";
    group = "mox";
    isNormalUser = true;
    createHome = true;
  };
  users.groups.mox = {};

  networking.firewall = {
    enable = true;
    allowedTCPPorts = [
      80
      443
      inputs.secrets.ssh-port
      inputs.secrets.woodpecker-grpc-port
      # mox
      465
      993
      25
      # matrix
      8448
    ];
    allowedTCPPortRanges = [
      {
        from = 49999;
        to = 59999;
      }
    ];
    allowedUDPPortRanges = [
      {
        from = 49999;
        to = 59999;
      }
    ];
    pingLimit = "--limit 1/s --limit-burst 3";
  };

  security.acme = {
    acceptTerms = true;
    defaults.email = inputs.secrets.acme-email;
  };


  services.nginx = {
    enable = true;

    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;

    sslCiphers = "AES256+EECDH:AES256+EDH:!aNULL";

    virtualHosts."box.kotatsu.dev" = {
      enableACME = true;
      forceSSL = true;

      locations."/" = {
        proxyPass = "http://127.0.0.1:${toString inputs.secrets.woodpecker-server-port}";
        proxyWebsockets = true;
        extraConfig = ''
          proxy_ssl_server_name on;
          proxy_pass_header Authorization;
        '';
      };
    };

    virtualHosts."git.kotatsu.dev" = {
      enableACME = true;
      forceSSL = true;

      locations."/" = {
        proxyPass = "http://127.0.0.1:${toString inputs.secrets.gitea-http-port}";
        extraConfig = ''
          proxy_ssl_server_name on;
          proxy_pass_header Authorization;
        '';
      };
    };

    virtualHosts."vault.kotatsu.dev" = {
      enableACME = true;
      forceSSL = true;

      locations."/" = {
        proxyPass = "http://127.0.0.1:${toString inputs.secrets.rocket-port}";
        extraConfig = ''
          proxy_ssl_server_name on;
          proxy_pass_header Authorization;
        '';
      };
    };

    virtualHosts."rss.kotatsu.dev" = {
      enableACME = true;
      forceSSL = true;

      locations."/" = {
        proxyPass = "http://127.0.0.1:${toString inputs.secrets.miniflux-port}";
        extraConfig = ''
          proxy_ssl_server_name on;
          proxy_pass_header Authorization;
        '';
      };

      locations."/fonts/ibm-plex/" = let
        ibm-plex-webfonts = self.packages.${pkgs.system}.ibm-plex-webfonts;
      in {
        alias = "${ibm-plex-webfonts}/";
        # Set Cache-Control to one week.
        extraConfig = ''
          add_header Cache-Control "public, max-age=25200";
        '';
      };

      locations."/fonts/iosevka/" = let
        iosevka-webfonts = self.packages.${pkgs.system}.iosevka-webfonts;
      in {
        alias = "${iosevka-webfonts}/";
        # Set Cache-Control to one week.
        extraConfig = ''
          add_header Cache-Control "public, max-age=25200";
        '';
      };
    };

    virtualHosts."s.kotatsu.dev" = {
      enableACME = true;
      forceSSL = true;

      locations."/" = {
        proxyPass = "http://127.0.0.1:${toString inputs.secrets.searx-port}";
      };
    };

    virtualHosts."matrix.kotatsu.dev" = {
      enableACME = true;
      forceSSL = true;

      listen = [
        { addr = "0.0.0.0"; port = 443; ssl = true; }
        { addr = "0.0.0.0"; port = 8448; ssl = true; }
      ];

      locations."/" = {
        return = "301 https://web.matrix.kotatsu.dev";
      };

      locations."/_matrix/" = {
        proxyPass = "http://127.0.0.1:6167$request_uri";
        proxyWebsockets = true;
        extraConfig = ''
          proxy_set_header Host $host;
          proxy_buffering off;
          proxy_ssl_server_name on;
          proxy_pass_header Authorization;
        '';
      };

      locations."=/.well-known/matrix/server" = {
        alias = pkgs.writeText "well-known-matrix-server" ''
          {
            "m.server": "matrix.kotatsu.dev"
          }
        '';

        extraConfig = ''
          default_type application/json;
        '';
      };

      locations."=/.well-known/matrix/client" = {
        alias = pkgs.writeText "well-known-matrix-server" ''
          {
            "m.homeserver": {
              "base_url": "https://matrix.kotatsu.dev"
            }
          }
        '';

        extraConfig = ''
          default_type application/json;
          add_header Access-Control-Allow-Origin "*";
        '';
      };
    };

    virtualHosts."mta-sts.mail.kotatsu.dev" = {
      enableACME = true;
      forceSSL = true;

      locations."/" = {
        proxyPass = "http://127.0.0.1:81";
        extraConfig = ''
          proxy_ssl_server_name on;
          proxy_pass_header Authorization;
        '';
      };
    };

    virtualHosts."autoconfig.mail.kotatsu.dev" = {
      enableACME = true;
      forceSSL = true;

      locations."/" = {
        proxyPass = "http://127.0.0.1:81";
        extraConfig = ''
          proxy_ssl_server_name on;
          proxy_pass_header Authorization;
        '';
      };
    };

    virtualHosts."mail.kotatsu.dev" = {
      enableACME = true;
      forceSSL = true;

      locations."/" = {
        proxyPass = "http://127.0.0.1:1080";
        extraConfig = ''
          proxy_ssl_server_name on;
          proxy_pass_header Authorization;
        '';
      };
    };
  };

  services.woodpecker-server = {
    enable = true;

    environmentFile = "/secrets/woodpecker-server.env";
    environment = {
      WOODPECKER_ADMIN = "kotatsuyaki";
      WOODPECKER_HOST = "https://box.kotatsu.dev";

      WOODPECKER_GITEA = "true";
      WOODPECKER_GITEA_URL = "https://codeberg.org";

      WOODPECKER_GRPC_ADDR = ":${toString inputs.secrets.woodpecker-grpc-port}";
      WOODPECKER_SERVER_ADDR = ":${toString inputs.secrets.woodpecker-server-port}";
    };
  };

  services.ensure-secrets.woodpecker-server = {
    required-by = [ "woodpecker-server.service" ];
    filename = "/secrets/woodpecker-server.env";
    variable-names = [
      "WOODPECKER_GITEA_CLIENT"
      "WOODPECKER_GITEA_SECRET"
      "WOODPECKER_AGENT_SECRET"
    ];
  };

  services.gitea = {
    enable = true;
    package = pkgs.forgejo;
    appName = "kotatsuyaki's git server";

    settings = {
      server = {
        HTTP_PORT = inputs.secrets.gitea-http-port;
        DOMAIN = "git.kotatsu.dev";
        ROOT_URL = "https://git.kotatsu.dev/";
        SSH_PORT = inputs.secrets.ssh-port;
      };
      service = {
        DISABLE_REGISTRATION = true;
      };
    };
  };

  services.vaultwarden = {
    enable = true;
    config = {
      DOMAIN = "https://vault.kotatsu.dev";
      SIGNUPS_ALLOWED = false;
      ROCKET_ADDRESS = "127.0.0.1";
      ROCKET_PORT = inputs.secrets.rocket-port;
    };
  };

  services.miniflux = {
    enable = true;
    adminCredentialsFile = "/secrets/miniflux.env";
    config = {
      LISTEN_ADDR = "localhost:${toString inputs.secrets.miniflux-port}";
      # Prevent aggresive cleaning of read articles.
      # Otherwise, elfeed fails to fetch new articles.
      # See https://github.com/fasheng/elfeed-protocol/issues/27.
      CLEANUP_ARCHIVE_READ_DAYS = "180";
      # Poll feeds every 360 minutes (i.e. 6 hours).
      POLLING_FREQUENCY = "360";
    };
  };
  users.groups.postgres.members = [ "miniflux" ];

  systemd.services.mox = {
    wantedBy = [ "multi-user.target" ];
    after = [ "network.target" ];
    description = "Mox mail server service";
    serviceConfig = {
      UMask = "007";
      LimitNOFILE = "65535";
      Type = "simple";
# Mox starts as root, but drops privileges after binding network addresses.
      WorkingDirectory = "/home/mox";
      ExecStart = "${pkgs.mox}/bin/mox serve";
      RestartSec = "5s";
      Restart = "always";
      ExecStop = "${pkgs.mox}/bin/mox stop";

# Isolate process, reducing attack surface.
      PrivateDevices = true;
      PrivateTmp = true;
      ProtectSystem = "strict";
      ReadWritePaths  =  [ "/home/mox/config" "/home/mox/data" ];
      ProtectKernelTunables = true;
      ProtectControlGroups = true;
      AmbientCapabilities = [];
      CapabilityBoundingSet  =  [ "CAP_SETUID" "CAP_SETGID" "CAP_NET_BIND_SERVICE" "CAP_CHOWN" "CAP_FSETID" "CAP_DAC_OVERRIDE" "CAP_DAC_READ_SEARCH" "CAP_FOWNER" ];
      RestrictAddressFamilies  =  [ "AF_INET" "AF_INET6" "AF_UNIX" "AF_NETLINK" ];
      NoNewPrivileges = true;
      ProtectProc = "invisible";
      RestrictNamespaces = true;
      RestrictRealtime = true;
      RemoveIPC = true;
      ProtectHostname = true;
      ProtectClock = true;
      ProtectKernelLogs = true;
      ProtectKernelModules = true;
      MemoryDenyWriteExecute = true;
      LockPersonality = true;
      DevicePolicy = "closed";
      SystemCallArchitectures = "native";
      SystemCallFilter = "@system-service";
    };
  };
}
