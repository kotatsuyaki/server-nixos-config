{ fetchzip }:

# The unpacked directory contains ./Iosevka.css.

fetchzip {
  name = "iosevka-webfonts";
  url = "https://github.com/be5invis/Iosevka/releases/download/v28.0.3/PkgWebFont-Iosevka-28.0.3.zip";
  sha256 = "sha256-/qzVQn2W2znXdLc98IXSpN6oHNwfHM08ZHqmL3srFyU=";
  stripRoot = false;
}
