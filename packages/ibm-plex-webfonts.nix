{ fetchzip }:

# The unpacked directory is like the following:
# .
# ├── CHANGELOG.md
# ├── css
# │   ├── ibm-plex-sans-jp.css
# │   ├── ibm-plex-sans-jp.min.css
# │   ├── ibm-plex-sans-kr.css
# │   ├── ibm-plex-sans-kr.min.css
# │   ├── ibm-plex.css
# │   └── ibm-plex.min.css
# ├── IBM-Plex-Mono
# ├── IBM-Plex-Sans
# ├── IBM-Plex-Sans-Arabic
# ├── IBM-Plex-Sans-Condensed
# ├── IBM-Plex-Sans-Devanagari
# ├── IBM-Plex-Sans-Hebrew
# ├── IBM-Plex-Sans-JP
# ├── IBM-Plex-Sans-KR
# ├── IBM-Plex-Sans-Thai
# ├── IBM-Plex-Sans-Thai-Looped
# ├── IBM-Plex-Serif
# ├── LICENSE.txt
# └── scss
fetchzip {
  name = "ibm-plex-webfonts";
  url = "https://github.com/IBM/plex/releases/download/v6.3.0/Web.zip";
  sha256 = "sha256-SYFbTjRtAFSB763jHny+NfQrUmftfBBmNqay7yArd58=";
}
