{ pkgs, lib, config, ... }:

with lib;

let
  cfg = config.services.ensure-secrets;

  mkCheckEnvLine = filename: env: ''
    if [ -z ''${${env}+x} ]; then echo "${env} is not set in file ${filename}"; exit 1; fi
  '';
  mkCheckEnvScriptText =
    filename: envs: ''
      source ${filename}
    '' + (builtins.concatStringsSep "" (map (env: mkCheckEnvLine filename env) envs)) + ''
      echo "Secrets file ${filename} check done"
    '';
  mkCheckEnvScript =
    filename: envs: pkgs.writeShellScript "ensure-secrets" (mkCheckEnvScriptText filename envs);

  mkEnsureSecretService = name: fileCfg: {
    name = "ensure-secrets-${name}";
    value = {
      description = "Secret file ensuring service for file ${fileCfg.filename}.";
      requiredBy = fileCfg.required-by;
      serviceConfig = {
        Type = "oneshot";
        ExecStart = "${mkCheckEnvScript fileCfg.filename fileCfg.variable-names}";
      };
    };
  };
in
{
  options.services.ensure-secrets = lib.mkOption {
    type = lib.types.attrsOf (lib.types.submodule {
      options = {
        filename = lib.mkOption {
          type = lib.types.str;
          description = "The path to the environment file.";
        };
        variable-names = lib.mkOption {
          type = lib.types.listOf lib.types.str;
          description = "A list of variable names to check for in the environment file.";
        };
        required-by = lib.mkOption {
          type = lib.types.listOf lib.types.str;
          default = [ ];
          description = "A list of services that should be run after ensure-secrets.";
        };
      };
    });
    example = ''
      {
        myservice = {
          filename = "/secrets/myservice.env";
          variable-names = [ "ENVVAR_A" "ENVVAR_B" ];
          required-by = [ "myservice.service" ];
        };
      }
    '';
  };

  config = {
    systemd.services = lib.attrsets.mapAttrs' mkEnsureSecretService cfg;
  };

  meta.maintainers = with lib.maintainers; [ ];
}
