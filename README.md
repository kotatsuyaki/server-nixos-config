# Server NixOS Configs

## Build and Deploy

```sh
NIX_SSHOPTS="-i <keyfile> -p <port>" \
  nixos-rebuild --target-host <user>@<server> .#<server>
```

## Sensitive Data

This flake relies on the `inputs.secrets` private flake for semi-sensitive data like IP addresses
and open ports.  To deploy the NixOS configurations in this repo with your own secrets:

1. Replace the input with your own secrets flake, starting from a mimimal `flake.nix` file with
   the following content.

   ```nix
   {
     outputs = { };
   }
   ```
2. Repeatedly build and fill in missing values to the secrets flake.
